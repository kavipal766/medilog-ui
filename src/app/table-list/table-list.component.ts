import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import { GlobalService } from '../global.service';
import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';
import { DomSanitizer } from '@angular/platform-browser';
 


declare var $: any;

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
keysArray : any[] = [];
imgsrc :any;
pdfsrc : any;
loading : boolean =false;
enableModel : boolean =false;
  constructor(public http : Http,routes:Router,private domSanitizer:DomSanitizer,private ng4LoadingSpinnerService: Ng4LoadingSpinnerService,public globalService:GlobalService) {

  }
  // var filehash ="QmW9H9sASoyAfgJFgX6KcNbWxsTF6aezZSBAuuHjCNbMYu";
  view(item,index){
    this.loading = true;
    this.imgsrc= '';
    this.pdfsrc= '';
   
    this.enableModel = true;
    $('#noticeModal').modal();
      this.http.post(this.globalService.basepath+'api/v1/medilocks/getLogs',{filehash:item.bill}).subscribe((res)=>{
        this.ng4LoadingSpinnerService.show();
        debugger
         if(res.json().status===200){
           this.loading = false;
           // this.pdfsrc = res.json().data

           
         if(res.json().data.split(',')[0]==='data:application/pdf:base64'){
            // this.pdfsrc = res.json().data
            
           this.pdfsrc = new Blob([res.json().data], {type: 'application/pdf'});
            //let PDFURL = URL.createObjectURL(currentBlob);
           // this.pdfsrc = this.domSanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(PDFURL));

         }else{
          this.imgsrc = res.json().data; 
         }
         this.ng4LoadingSpinnerService.hide();
         }else{
         this.showNotification('top','right',4,res.json().message);
         }
         });
  }

  delete(item,index){
    this.http.post(this.globalService.basepath+'api/v1/medilocks/deletelog',{userid:item._id}).subscribe((res)=>{
      this.ng4LoadingSpinnerService.show();
      debugger
       if(res.json().status===200){
        this.showNotification('top','right',2,res.json().message);
        this.ngOnInit();
       }else{
       this.showNotification('top','right',4,res.json().message);
       }
       });
  }

  ngOnInit() {
  this.loading=true;
    var user = localStorage.getItem('currentUser');
    var userData = JSON.parse(user);
    const url=this.globalService.basepath+'api/v1/medilocks/getAllLogs'
    this.http.post(url,{userId : userData._id}).subscribe((res)=>{
  		this.loading=false;
      if(res.json().status===200){
  			this.keysArray=res.json().data;
  			// this.showNotification('top','right',2,res.json().message);

  		}else{
  			this.showNotification('top','right',4,res.json().message);
  			// alert(res.json().message);
  		}
  	});
  }


    showNotification(from, align,color1,msg){
      const type = ['','info','success','warning','danger'];

      const color = color1;

      $.notify({
          icon: "notifications",
          message: msg

      },{
          type: type[color],
          timer: 4000,
          placement: {
              from: from,
              align: align
          },
          template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
            '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
            '<i class="material-icons" data-notify="icon">notifications</i> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
              '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
      });
  }


}
